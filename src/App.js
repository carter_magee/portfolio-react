import React, { Component } from 'react'
import styled from 'styled-components'
import { Router, Route, Switch } from 'react-router-dom'
import history from './History'

import Transitions from './transitions'
import Nav from './components/Nav'
import About from './components/About'
import Gallery from './components/Gallery'
import Portfolio from './components/Portfolio'
import Skills from './components/Skills'
import Info from './components/Info'
import Contact from './components/Contact'
import NotFound from './components/NotFound'

import Projects from './data/Projects'
import Cats from './data/Cats'


const Wrapper = styled.main`
  display: grid;
  grid-template-columns: 140px auto;
  perspective: 1200px;
  overflow: hidden;
  max-height: 100vh;
`
class App extends Component {
  constructor(props) {
    super(props)
    this.test = this.test.bind(this)
    this.state = {
      projects: Projects,
      cats: Cats
    }
  }

  test() {
    let projObj = this.state.projects 
    console.log(projObj)
  }

  render(){

    return ( 
      <Router history={history}>
        <Wrapper>
          <Nav/>
          <Route
          render={({ location }) => (
            <Transitions pageKey={location.key} {...location.state}> 
            <Switch location={location}>
            <Route exact path="/" render={props => <Gallery projects={this.state.projects} cats={this.state.cats} test={this.test}/>}/>
            <Route path="/portfolio" render={props => <Portfolio projects={this.state.projects} cats={this.state.cats} test={this.test} />} />
            <Route path="/about" component={About} />
            <Route path="/skills" component={Skills} />
            <Route path="/info" component={Info} />
            <Route path="/contact" component={Contact} />
            <Route component={NotFound} />
            </Switch>
            </Transitions>
          )}/>
        </Wrapper>
      </Router>
    )
  }
}

export default App