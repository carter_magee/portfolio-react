export function random(upper, lower) {
    return Math.floor(Math.random() * upper) + lower;
}

export function dimensions() {
        const digits = Array.from({ length: 10 }, () => random(5))
    return digits
}

