const Projects = [
    {
        "name": "lostinsound.live",
        "theme": "jquery",
        "imgPrev": "https://i.imgur.com/jfZjlfI.png",
        "brief": "lostinsound.live live streaming aggregation platform",
        "desc": "Welcome to the lostinsound.live live streaming aggregation platform. Sign in and tune in to live streaming video and audio content, chat with other users, and interact with the streamers. This page is populated by admins filling out a Google form detailing information about upcoming streaming events. Each time the form is submitted, a Google script creates a Google calendar event and formats the responses into a JSON object which is passed to the description section of the event. Our backend then queries for and collects each event's data. The event object from the description, as well as user data, is then passed to the frontend through AJAX promises. The data from the event and users are both logged in a Google spreadsheet and used to populate the page appropriately based on the kind of event",
        "url": "http://www.lostinsound.live/",
        "gitlab": "",
        "icons": {
            "1": "http://icons.iconarchive.com/icons/cornmanthe3rd/plex/256/Other-html-5-icon.png",
            "2": "https://i.imgur.com/Z493CXH.png",
            "3": "http://icons.iconarchive.com/icons/sicons/basic-round-social/256/jquery-icon.png",
            "4": "http://flask.pocoo.org/static/logo/flask.png",
            "5": "https://i.imgur.com/w5DYH6y.png",
        },
        "flipped": false,
        "id": 1
    },
    {
        "name": "Tangible Hope Foundation",
        "theme": "wordpress",
        "imgPrev": "https://i.imgur.com/vnzXktc.png",
        "brief": "null", 
        "desc": "Tangible Hope Foundation is a community-based non-profit organization that empowers and protects young girls living in poverty in rural Ethiopia by advancing their rights through education, medical care and nutrition.  It is our goal that this generation of women, in the world, will inspire and lead those around them and the next generation out of poverty and into a world of opportunity. ",
        "url": "http://www.tangiblehope.org/",
        "gitlab": "",
        "icons": {
            "1": "http://icons.iconarchive.com/icons/cornmanthe3rd/plex/256/Other-html-5-icon.png",
            "2": "https://i.imgur.com/Z493CXH.png",
            "3": "https://upload.wikimedia.org/wikipedia/en/thumb/9/9e/JQuery_logo.svg/524px-JQuery_logo.svg.png",
            "4": "http://icons.iconarchive.com/icons/martz90/circle/256/wordpress-icon.png",
            "5": "http://icons.iconarchive.com/icons/graphics-vibe/developer/256/php-icon.png",
        },
        "flipped": false,
        "id": 2
    },
    {
        "name": "simon",
        "theme": "angular",
        "imgPrev": "https://i.imgur.com/Gu2Syk4.png",
        "brief": "remake of the classic Simon memory game!",
        "desc": "pair programming, AngularJS, Ruby on Rails, SASS",
        "url": "https://simonsaidwhat.herokuapp.com/",
        "gitlab": "https://github.com/cartermagee/simon",
        "icons": {
            "1": "https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Angular_full_color_logo.svg/512px-Angular_full_color_logo.svg.png",
            "2": "https://upload.wikimedia.org/wikipedia/commons/thumb/9/96/Sass_Logo_Color.svg/512px-Sass_Logo_Color.svg.png",
            "3": "https://upload.wikimedia.org/wikipedia/en/thumb/9/9e/JQuery_logo.svg/524px-JQuery_logo.svg.png",
            "4": "https://upload.wikimedia.org/wikipedia/commons/thumb/6/62/Ruby_On_Rails_Logo.svg/411px-Ruby_On_Rails_Logo.svg.png",
            "5": "https://upload.wikimedia.org/wikipedia/commons/thumb/2/29/Postgresql_elephant.svg/540px-Postgresql_elephant.svg.png",
            "6": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/73/Ruby_logo.svg/198px-Ruby_logo.svg.png",
        },
        "flipped": false,
        "id": 3
    },
    {
        "name": "South Park Memory",
        "theme": "jquery",
        "imgPrev": "https://i.imgur.com/RB934mi.png",
        "brief": "South Park themed remake of the classic memory game!",
        "desc": "null",
        "url": "https://cartermagee.github.io/South-Park-Memory/",
        "gitlab": "https://github.com/cartermagee/South-Park-Memory",
        "icons": {
            "1": "http://icons.iconarchive.com/icons/cornmanthe3rd/plex/256/Other-html-5-icon.png",
            "2": "https://i.imgur.com/Z493CXH.png",
            "3": "http://icons.iconarchive.com/icons/sicons/basic-round-social/256/jquery-icon.png",
            "4": "https://i0.wp.com/blog.fossasia.org/wp-content/uploads/2017/07/handlebars-js.png?fit=500%2C500&ssl=1&resize=350%2C200",
            "5": "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Node.js_logo.svg/590px-Node.js_logo.svg.png",
            "6": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/72/Gulp.js_Logo.svg/175px-Gulp.js_Logo.svg.png",
        },
        "flipped": false,
        "id": 4
    },
    {
        "name": "OpenScores",
        "theme": "react",
        "imgPrev": "https://i.imgur.com/kJYIrfD.png",
        "brief": "Open Scores is a reactjs collaboration tool for composers allowing users to simultaneously draft music notation in real time.",
        "desc": "Open Scores is a collaboration tool for composers built in ReactJS allowing users to simultaneously draft music notation in real time. Rendering and manipulation of musical notation was achieved through integration with VexFlow. Playback of scores is deliverd with ToneJS. Storage and recall of data is provided by syncing with Google Firebase.",
        "url": "",
        "gitlab": "https://github.com/cartermagee/OpenScores",
        "icons": {
            "1": "http://icons.iconarchive.com/icons/cornmanthe3rd/plex/256/Other-html-5-icon.png",
            "2": "https://i.imgur.com/Z493CXH.png",
            "3": "http://icons.iconarchive.com/icons/sicons/basic-round-social/256/jquery-icon.png",
            "4": "https://i0.wp.com/blog.fossasia.org/wp-content/uploads/2017/07/handlebars-js.png?fit=500%2C500&ssl=1&resize=350%2C200",
            "5": "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Node.js_logo.svg/590px-Node.js_logo.svg.png",
            "6": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/72/Gulp.js_Logo.svg/175px-Gulp.js_Logo.svg.png",
        },
        "flipped": false,
        "id": 5
    },
    {
        "name": "5 Points Music Sanctuary",
        "theme": "wordpress",
        "imgPrev": "https://i.imgur.com/oqaBZAH.png",
        "brief": "UI and SEO for 5 Points Music Sanctuary",
        "desc": "",
        "url": "https://5pointsmusic.com/",
        "gitlab": "",
        "icons": {
            "1": "http://icons.iconarchive.com/icons/cornmanthe3rd/plex/256/Other-html-5-icon.png",
            "2": "https://i.imgur.com/Z493CXH.png",
            "3": "https://upload.wikimedia.org/wikipedia/en/thumb/9/9e/JQuery_logo.svg/524px-JQuery_logo.svg.png",
            "4": "http://icons.iconarchive.com/icons/martz90/circle/256/wordpress-icon.png",
            "5": "http://icons.iconarchive.com/icons/graphics-vibe/developer/256/php-icon.png",
        },
        "flipped": false,
        "id": 5
    }
]


export default Projects;