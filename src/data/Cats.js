const Cats = [
    {
        "name": "zoe",
        "image": "https://i.imgur.com/DkYl0YO.jpg",
        "desc": "Zoe is a big, bad, and buoyent, 12 year old, Calico I adopted fom an old bandmate while I was living in Ashville, NC. Her hobbies include complaining, knocking over water cups, Gravy Lovers wet cat food, vanilla ice cream, mayonaise, ham, Salmon oil based CBD cat treats(for her arthritis), destroying leather furniture, pooping at inconvenient times, and most importantly being sweet.",
        "flipped": false,
        "id": 1
    },
    {
        "name": "zoe",
        "image": "https://i.imgur.com/oEYK4C7.jpg",
        "desc": "Zoe is a big, bad, and buoyent, 12 year old, Calico I adopted fom an old bandmate while I was living in Ashville, NC. Her hobbies include complaining, knocking over water cups, Gravy Lovers wet cat food, vanilla ice cream, mayonaise, ham, Salmon oil based CBD cat treats(for her arthritis), destroying leather furniture, pooping at inconvenient times, and most importantly being sweet.",
        "flipped": false,
        "id": 2
    },
    {
        "name": "zoe",
        "image": "https://i.imgur.com/ybZVbos.jpg",
        "desc": "Zoe is a big, bad, and buoyent, 12 year old, Calico I adopted fom an old bandmate while I was living in Ashville, NC. Her hobbies include complaining, knocking over water cups, Gravy Lovers wet cat food, vanilla ice cream, mayonaise, ham, Salmon oil based CBD cat treats(for her arthritis), destroying leather furniture, pooping at inconvenient times, and most importantly being sweet.",
        "flipped": false,
        "id": 3
    },
    {
        "name": "zoe",
        "image": "https://i.imgur.com/WTUuAhJ.jpg",
        "desc": "Zoe is a big, bad, and buoyent, 12 year old, Calico I adopted fom an old bandmate while I was living in Ashville, NC. Her hobbies include complaining, knocking over water cups, Gravy Lovers wet cat food, vanilla ice cream, mayonaise, ham, Salmon oil based CBD cat treats(for her arthritis), destroying leather furniture, pooping at inconvenient times, and most importantly being sweet.",
        "flipped": false,
        "id": 4
    },
    {
        "name": "Jeffrey",
        "image": "https://i.imgur.com/5XleCd7.jpg",
        "desc": "Jeffrey is a 5 tear old Manx with a little tail and big ears who I adopted while I was living in Asheville, NC. His hobbies include bringing me dead animals, eating bugs. He knows several dog tricks such as low/high five and fetching champagne corks.",
        "flipped": false,
        "id": 1
    },
    {
        "name": "Jeffrey",
        "image": "https://i.imgur.com/kNxLVzY.jpg",
        "desc": "Jeffrey is a 5 tear old Manx with a little tail and big ears who I adopted while I was living in Asheville, NC. His hobbies include bringing me dead animals, eating bugs. He knows several dog tricks such as low/high five and fetching champagne corks.",
        "flipped": false,
        "id": 2
    },
    {
        "name": "Jeffrey",
        "image": "https://i.imgur.com/0Z1AQho.jpg",
        "desc": "Jeffrey is a 5 tear old Manx with a little tail and big ears who I adopted while I was living in Asheville, NC. His hobbies include bringing me dead animals, eating bugs. He knows several dog tricks such as low/high five and fetching champagne corks.",
        "flipped": false,
        "id": 3
    },
]


export default Cats;