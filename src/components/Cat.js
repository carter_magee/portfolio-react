import React, {Component} from 'react'
import styled from 'styled-components'
import {random} from '../helper'


const CatStyled = styled.div`
    background: #fff;
    display: grid;
    overflow: hidden;
    color: #fff;
    grid-template-columns: 1;
    grid-template-rows: 1;
    border: 1px solid purple;
    width: 100%;
`

const Image = styled.img`
    grid-column: 1 / -1;
    grid-row: 1 / -1;
    width: 100%;
    object-fit: cover;
`

class Cat extends Component {
    render() {
        const {details} = this.props
  
        // console.log(details)
        const images = details.image
        
        return (
            <CatStyled className={`span-${random(2,1)}`}>
                <Image src={images}/>
            </CatStyled>
        )
    }
}

export default Cat