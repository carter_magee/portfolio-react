import React from 'react'
import styled from 'styled-components'
import history from '../History'
import { slide } from '../transitions'

const NotFoundStyled = styled.section`
  background: #fff;
  color: #000;
  display: grid;
  justify-items: center;
  align-items: center;
`
const BackContainer = styled.div`
    display: grid;
    justify-items: center;
    align-items: center;
`

const BackBtn = styled.button`
    width: 150px;
    border-radius: 5px;
    color: #fff;
    background: #494949;
`

const NotFound = () => {
    return (
        <NotFoundStyled>
            <BackContainer>
                <h1>Not Found</h1>
                <h2>the page you are looking for doesn't exist</h2>
                <BackBtn onClick={() => history.push({ pathname: '/', state: slide })}>back to safety</BackBtn>
            </BackContainer>
        </NotFoundStyled>
    );
};

export default NotFound;