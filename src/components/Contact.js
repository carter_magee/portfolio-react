import React from 'react'
import styled from 'styled-components'

const ContactStyled =  styled.section`
  background: blanchedalmond;
`

const Contact = () => {
  return (
    <ContactStyled className="contact-container">
      Contact
    </ContactStyled>
  );
};

export default Contact;