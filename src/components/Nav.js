import React, {Component} from 'react';
import styled from 'styled-components'
import mixer from '../images/icons/mixer.png';
import history from '../History'
import { slide } from '../transitions'

const StyledNav = styled.aside`
  display: grid;
  grid-row-gap: 5px;
  grid-template-rows: 64px 5fr 1fr;
  height: 100vh;
  background: #222;
  z-index: 3;
`
const MainNav = styled.nav`
  font-size: 21px; 
  display: grid;
  align-items: stretch;
  /* overflow: hidden; */
  justify-items: stretch;
  grid-template-rows: repeat(5, 1fr);
  height: 100%;
`
const NameContainer = styled.button`
  letter-spacing: 1.5px;
  font-weight: 600;
  text-transform: uppercase;
  display: grid;
  color: #3ED863 !important;
  background: #333; 
  text-align: center;
  align-items: center;
  &:hover {
    background: #39D872;
    color: #fff !important;      
  }
`

const Icon = styled.img`
    width: 50%;
    filter: invert(100%);
`

const NavItem = styled.button`
  &:hover {
    background: #39D872;
    font-weight: 600;
    text-shadow: 1px 1px #3b4199;
  }
  &:hover ${Icon} {
    filter: invert(100%) drop-shadow(1px 1px 1px #3b4199);
  }
  &.active {
  background: #494949;
  color: #39D872;
  font-weight: 600;
  }
`
// const Logo = styled.div`
//   text-align: center;
//   display: grid;
//   align-items: center;
//   justify-items: center;
// `

class Nav extends Component {
    constructor(props) {
      super(props)
      this.toggleActiveClass= this.toggleActiveClass.bind(this);
      this.removeActiveClass= this.removeActiveClass.bind(this);
      this.state = {
          active: ''
      };
    }

    removeActiveClass() {
      const active = '';
      this.setState({active});
    }
    
    toggleActiveClass(i) {
      const active = i;
      this.setState({active});
    }

  render() {
    return (
      <StyledNav>
        <NameContainer 
          onClick={()=> {
            this.removeActiveClass()
            history.push({pathname: '/', state: slide})
          }} role="button">
        Carter Magee
        </NameContainer>
        <MainNav>
          <NavItem 
            onClick={()=> {
              this.toggleActiveClass(0)
              history.push({pathname: '/portfolio', state: slide})
            }} 
            className={(this.state.active === 0) ? "active" : ""}>
            <Icon src={mixer} alt=""/>
            <li>portfolio</li>
          </NavItem>

          <NavItem 
            onClick={()=> {
              this.toggleActiveClass(1)
              history.push({pathname: '/about', state: slide})
            }} 
            className={(this.state.active === 1) ? "active" : ""}>
            <Icon src={mixer} alt=""/>
            <li>about</li>
          </NavItem>
          <NavItem 
            onClick={()=> {
              this.toggleActiveClass(2)
              history.push({pathname: '/skills', state: slide})
              }} 
            className={(this.state.active === 2) ? "active" : ""}>
            <Icon src={mixer} alt=""/>
            <li>skills</li>
          </NavItem>

          <NavItem 
            onClick={()=> {
              this.toggleActiveClass(3)
              history.push({pathname: '/info', state: slide})
              }} 
            className={(this.state.active === 3) ? "active" : ""}>
            <Icon src={mixer} alt=""/>
            <li>info</li>
          </NavItem>

            <NavItem 
              onClick={()=> {
                this.toggleActiveClass(4)
                history.push({pathname: '/contact', state: slide})
              }} 
            className={(this.state.active === 4) ? "active" : ""}>
            <Icon src={mixer} alt=""/>
            <li>contact</li>
            </NavItem>
        </MainNav>
        {/* <Logo>
          <Icon src={mixer} alt=""/>
        </Logo> */}
      </StyledNav>
    );
  }
}

export default Nav;
