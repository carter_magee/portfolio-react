import React, {Component} from 'react'
import styled from 'styled-components'
import GalleryItem from './GalleryItem'
import Cat from './Cat'

const GalleryStyled = styled.section`
  background: orange;
  grid-template-columns: repeat(auto-fill, 113px);
  grid-auto-rows: 68px;
  grid-auto-flow: dense;
  overflow: scroll;
`
class Gallery extends Component {
  render() {
    const {projects, cats, test} = this.props

    projects.sort(function () {
      return 0.5 - Math.random()
    });
    
    return (
      <GalleryStyled>
        {Object.keys(projects).map((key) => <GalleryItem key={key} index={key} details={projects[key]} test={test}/>)}
        {Object.keys(cats).map((key) => <Cat key={key} index={key} details={cats[key]}/>)}
      </GalleryStyled>
    )
  }
}

export default Gallery

