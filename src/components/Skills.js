import React from 'react';
import styled from 'styled-components'

const SkillsStyled = styled.section`
  background: peachpuff;
`


const Skills = () => {
  return (
    <SkillsStyled className="skills-container">
      Skills
    </SkillsStyled>
  );
};

export default Skills;
