import React, {Component} from 'react'
import styled, {injectGlobal} from 'styled-components'
import {random} from '../helper'

injectGlobal`
    .span-1{
        grid-column: span 1;
        grid-row: span 1;
    }
    .span-2{
        grid-column: span 2;
        grid-row: span 2;
    }
    .span-3{
        grid-column: span 3;
        grid-row: span 3;
    }
    .span-4{
        grid-column: span 4;
        grid-row: span 4;
    }
    .span-5{
        grid-column: span 5;
        grid-row: span 5;
    }
`
const Image = styled.img `
    cursor: pointer;
    grid-column: 1 / -1;
    grid-row: 1 / -1;
    object-fit: cover;
    width: 100%;
`
const ImgOverlay = styled.div `
    background: #30363352;
    display: grid;
    grid-column: 1 / -1;
    grid-row: 1 / -1;
    transition: 0.2s;
    transform: translateY(100%);
`

const GalleryItemStyled = styled.div`
    border: 1px solid red;
    color: #fff;
    display: grid;
    grid-template-columns: 1;
    grid-template-rows: 1;
    overflow: hidden;
    text-align: center;
    width: 100%;

    &:hover ${ImgOverlay} {
        transform: translateY(0);
    }
`
const ImgOverlayInner = styled.div`
    align-items: center;
    align-self: center;
    background: #494949;
    border-radius: 10px;
    display: grid;
    filter: drop-shadow(0px 0px 3px rgba(225, 225, 225, 0.9));    height: 47%;
    justify-items: center;
    justify-self: center;
    padding: 5px;
    width: 75%;
`
const BtnContainer = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-around;
    width: 210px;
`

const Btn = styled.button `
    align-items: center;
    background: #3ED863;
    border-radius: 5px;
    color: #fff;
    display: flex;
    font-size: 16px;
    font-weight: 600;
    justify-content: center;
    padding: 0 5px;
    text-align: center;
    &:hover {
        box-shadow: 1px 1px #3b4199;
        text-shadow: 1px 1px #3b4199;
        transform: scale(1.1);
    }
`

class GalleryItem extends Component {
    render() {
        const {details, test} = this.props
        return (
            <GalleryItemStyled className={`span-${random(3, 3)}`}>
                <Image src={details.imgPrev}/>
                <ImgOverlay>
                    <ImgOverlayInner>
                        <p>{details.name}</p>
                        <BtnContainer>
                            <Btn onClick={test}>Quick View</Btn>
                            <Btn onClick={test}>Visit Page</Btn>
                        </BtnContainer>
                    </ImgOverlayInner>
                </ImgOverlay>
            </GalleryItemStyled>
        )
    }
}

export default GalleryItem


