import React from 'react';
import styled from 'styled-components'

const AboutStyled = styled.section`
  background: red;
`

const About = () => {
  return (
    <AboutStyled className="about-container">
      ABOUT
    </AboutStyled>
  );
};

export default About;
