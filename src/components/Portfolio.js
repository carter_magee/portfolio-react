import React, { Component } from 'react'
import styled from 'styled-components'

const PortfolioStyled = styled.section`
  background: green;
`

class Portfolio extends Component {
  render() {
    return (
      <PortfolioStyled className="portfolio-container">
          portfolio
      </PortfolioStyled>
    )
  }
}

export default Portfolio