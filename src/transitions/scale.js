import { injectGlobal, keyframes } from 'styled-components'

const transitionClassName = 'scale'
const duration = 1000

const moveFromLeft = keyframes`
from { transform: translateX(-100%); }
`
const scaleDown = keyframes`
to { opacity: 0; transform: scale(.5); }
`

injectGlobal`
.${transitionClassName}-enter, .${transitionClassName}-exit {
  position: relative;
}
.${transitionClassName}-exit-active {
  animation: ${scaleDown} ${duration}ms ease both;
  z-index: 1;
}
.${transitionClassName}-enter-active {
  animation: ${moveFromLeft} ${duration}ms ease both;
  z-index: 2;
}
`

export default { transition: transitionClassName, duration }
