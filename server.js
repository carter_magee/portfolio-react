var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);

server.listen(8000);

io.on('connection', (socket) => {
  const room = socket.handshake['query']['room'];
  socket.join(room);

  socket.on('disconnect', function () {
    socket.leave(room);
    io.to(room).emit('remove user', socket.id);
  });

  socket.on('add user', (data) => {
    io.to(room).emit('add user', data);
  });

  socket.on('post message', (data) => {
    io.to(room).emit('post message', data);
  })

  socket.on('update board', (data) => {
    io.to(room).emit('update board', data);
  })
});